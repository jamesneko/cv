sourcefiles = src/CV-JamesClark.xml src/CV-JamesClark.xsl

all: pdf

pdf: $(sourcefiles)
	wkhtmltopdf --enable-local-file-access src/CV-JamesClark.xml bin/CV-JamesClark.pdf

clean:
	rm bin/*.pdf
