"use client";
import { useState, useEffect, useCallback } from "react";
import styles from "./links.module.css";

export default function OnlineLinks() {
  const [phonenum, setPhonenum] = useState();
  const d = new Date();
  const today = d.toISOString().match(/^(\d+-\d+-\d+)/)[0];
  const base = process.env.NEXT_PUBLIC_BASEPATH;

  const fetchPhonenum = useCallback(async () => {
    // You may see multiple of these getting set. That's because on desktop, we are doing Shenanigans, remember!
    if (phonenum) {
      console.log("Not fetching phone number: already set!");
    }
    console.log("Fetching phone number.");
    const res = await fetch("https://neko.stream/api/phonenum");
    if (!res.ok) {
      console.log("Couldn't get phone number: response not ok: response=", res);
      return;
    }
    const json = await res.json();
    const num = json?.num;
    if (!num) {
      console.log("Couldn't get phone number: no num: response=", res);
    } else if (num == phonenum) {
      console.log("Got phone number, but it was already set! response=", res);
    } else {
      console.log("Setting phone number:", num);
      setPhonenum(num);
    }
  }, []);
  useEffect(() => {
    fetchPhonenum().catch(console.error);
  }, [fetchPhonenum]);

  return (
    <div className={styles.links}>
      <ul>
        <li className={`${styles.text}`}>
          <a href="mailto:jobs@james-clark.au">
            <img className="icon" src={`${base}/email.svg`} alt="" />
            jobs@james-clark.au
          </a>
        </li>
        {phonenum && (
          <li className={`${styles.text}`}>
            <img className="icon" src={`${base}/phone.svg`} alt="" />
            {phonenum}
          </li>
        )}
        <li className={`${styles.text}`}>
          <a href="https://www.linkedin.com/in/james-ac-clark/">
            <img className="icon" src={`${base}/linkedin.svg`} alt="" />
            www.linkedin.com/in/james-ac-clark
          </a>
        </li>
        <li className={`${styles.text}`}>
          <a href={`${base}/james-clark.pdf`} download>
            <img className="icon" src={`${base}/pdf.svg`} />
            PDF download
          </a>
        </li>
        <li className={`${styles.text} ${styles.printOnly}`}>
          This print or PDF copy of the CV was generated on {today}. View the
          latest version online at &nbsp;
          <a href="https://james-clark.au/cv">james-clark.au/cv</a>.
        </li>
      </ul>
      <img
        alt="QR Code for james-clark.au/cv"
        src={`${base}/qrcode.svg`}
        className={styles.qr}
      />
    </div>
  );
}
