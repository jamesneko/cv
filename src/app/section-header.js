export default function SectionHeader({ name, children }) {
  return (
    <div className="sectionHeaderWithFirstSection">
      <h2>{name}</h2>
      {children}
    </div>
  );
}
