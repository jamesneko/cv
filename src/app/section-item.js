const OptionalPaperBit = ({ value }) => {
  if (value) {
    return (
      <>
        {", "}
        <span>{value}</span>
      </>
    );
  } else {
    return "";
  }
};

export default function SectionItem({
  type,
  name,
  description,
  where,
  when,
  hook,
  icon,
  details = [],
  keywords,
  authors,
  year,
  publication,
  abbreviation,
  pageRef,
  press,
  extraRef,
}) {
  const base = process.env.NEXT_PUBLIC_BASEPATH;
  if (type === "SKILL") {
    return (
      <section className="skill">
        {icon && <img className="icon" src={`${base}/${icon}`} alt="" />}
        <div>
          <h3>{name}</h3>
          <p className="description">{description}</p>
        </div>
      </section>
    );
  } else if (type === "JOB" || type === "EDUCATION") {
    return (
      <section className="experience">
        <div className="sectionHeader">
          <div className="sectionTitleRow">
            <h3>{name}</h3>
            <div className="whenAndWhere">
              <span className="where">{where}</span>
              <span className="period">{when.from + " - " + when.to}</span>
            </div>
            {icon && <img className="icon" src={`${base}/${icon}`} alt="" />}
          </div>
          <p className="hook">{hook}</p>
        </div>
        <ul>
          {details.map((detail, i) => (
            <li key={`detail${i}`}>{detail}</li>
          ))}
        </ul>
        {keywords && (
          <ul className="keywords">
            {keywords.map((keyword, i) => (
              <li key={`keyword${i}`}>{keyword}</li>
            ))}
          </ul>
        )}
      </section>
    );
  } else if (type === "PUBLICATION") {
    return (
      <section className="publication">
        {`${authors} (${year}). `}
        <span className="pubTitle">{`"${name}"`}</span>
        {`, in `}
        <span className="publicationName">{publication}</span>
        <OptionalPaperBit value={abbreviation && `${abbreviation}`} />
        <OptionalPaperBit value={where} />
        <OptionalPaperBit value={pageRef} />
        <OptionalPaperBit value={press} />
        <OptionalPaperBit value={extraRef} />.
      </section>
    );
  } else {
    throw new Error(`Uhhhh unhandled SectionItem type ${type}`);
  }
}
