export function createSections(cv) {
  const sections = {};
  cv.items?.forEach((item) => {
    if (item.type) {
      sections[item.type] ??= [];
      sections[item.type].push(item);
    }
  });
  return {
    ...cv,
    sections,
  };
}
