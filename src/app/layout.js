import { Roboto } from "next/font/google";
import "./globals.css";

const robotoFont = Roboto({ weight: ["400", "700"], subsets: ["latin"] });

export const metadata = {
  title: "CV: James Clark", // Oh no, what if our name changes? We would have to update it in multiple places! That's not best practice!
  description: "CV for James Clark",
};

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <head>
        <script
          async
          src="https://www.googletagmanager.com/gtag/js?id=G-KF3R6DSZTY"
        ></script>
        <script
          dangerouslySetInnerHTML={{
            __html: `
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
          
            gtag('config', 'G-KF3R6DSZTY');
            `,
          }}
        />
        <meta name="title" property="og:title" content="James Clark's CV" />
        <meta name="type" property="og:type" content="website" />
        <meta
          name="description"
          property="og:description"
          content="Software developer with a broad range of skills from frontend to backend."
        />
      </head>
      <body className={robotoFont.className}>{children}</body>
    </html>
  );
}
