<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" encoding="utf-8"/>

<!-- ======================================================================= -->
<xsl:template match="cv">
<!-- ======================================================================= -->
<html>
<head>
	<title> <xsl:value-of select="name"/> </title>
	<style type="text/css">
		@page {
			margin-top: 2cm;
			margin-bottom: 2cm;
			margin-left: 2cm;
			margin-right: 2cm;
		}
		
		.rightsmallinfo {
			font-family: 'Bitstream Vera Sans';
			font-size: 10pt;
			margin-top: 0cm;
			margin-bottom: 0cm;
			text-align: right;
			font-style: italic;
			font-weight: normal;
		}
		.right-info-block {
			/*float: right; causes .applicant-title to not be centred anymore. what am I doing wrong? */
		}
		
		.applicant-title {
			font-family: 'Bitstream Vera Sans';
			font-size: 18pt;
			margin-top: 0.42cm;
			margin-bottom: 0.20cm;
			font-weight: bold;
			text-align: center;
		}
		
		.heading {
			font-family: 'Bitstream Vera Sans';
			font-size: 12pt;
			margin-top: 0.42cm;
			margin-bottom: 0.10cm;
			font-weight: bold;
		}

		.subjectborder {
			page-break-inside: avoid;
			page-break-after: avoid;
			margin-top: 1.0cm;
			margin-bottom: 0.20cm;
			padding: 0cm;
			border-top: 1px solid black;
			border-left: 1px solid black;
		}
		.subject { 
			margin: 0cm;
			padding: 0.08cm;
			font-family: 'Bitstream Vera Sans';
			font-size: 14pt;
			background-color: #cccccc;
			font-style: normal;
			font-weight: bold;
		}
		
		.page-break-inhibitor {
			page-break-inside: avoid;
		}

		.subheading {
			font-family: 'Bitstream Vera Sans';
			font-size: 10pt;
			margin-top: 0cm;
			margin-bottom: 0.20cm;
			font-style: italic;
			font-weight: normal;
		}

		div.content {
			page-break-inside: avoid;
			break-inside: avoid;
			margin: 0.5cm;
		}
		div.with-divider {
			border-bottom: 1px dashed;
		}
		div.with-divider:last-child {
			/* border-bottom: 5px dashed red;	 hmm. */
		}
		
		.yellowbg {
			background-color: #cfc5a7
		}
		.redbg {
			background-color: #cfaaa7
		}
		.bluebg {
			background-color: #a7b6cf
		}
		.greenbg {
			background-color: #a7cfb2
		}

		ul { 
			padding-left: 1cm;
		}
		div>p {		/* Make paras at the 'top level' work much the same as an ul */
			padding-left: 1cm;
		}
		ul.item {}
		li.item {}
		ul.horizontal {
			display: inline-block;
		}
		li.horizontal {
			margin-right: 0.5em;
			display: inline-block;
		}
		li.horizontal::after {
			content: ",";
		}
		li.horizontal:last-child::after {
			content: ".";
		}
		
		.summary ul { margin-top: 0cm; margin-bottom: 0cm; }
		.summary p  { margin-top: 0cm; margin-bottom: 0cm; }
		
		span.pubpaper { font-style: italic; }
		div.pub span::after {
			content: ",";
		}
		div.pub span.pubauthors::after, div.pub span.pubyear::after {
			content: " ";
		}
		div.pub span:last-child::after {
			content: ".";
		}

		.endblock {
			border-top: 1px dashed;
			padding-top: 0.5cm;
			font-size: 10pt;
			font-style: italic;
		}
		
		/* Since wkhtmltopdf requires a patched Qt to render hyperlinks into working links in the PDF, and we don't have that,
		 * just render them normally if we're "printing".
		 */
		@media print {
			a { color: black; text-decoration: none }
		}
	</style>
</head>
<body>

	<div class="right-info-block">
		<p class="rightsmallinfo"> Email <a href="mailto:{email}"><xsl:value-of select="email"/></a> </p>
	</div>
	
	<h1 class="applicant-title"> <xsl:value-of select="name"/> </h1>


	<!-- =================== SUMMARY ====================== -->
	<div class="subjectborder"><h2 class="subject heading">Summary</h2></div>
	<xsl:call-template name="summary"/>		<!-- somehow I doubt we'll run into summary page break issues -->

	<!-- =================== EMPLOYMENT =================== -->
	<div class="page-break-inhibitor">
		<div class="subjectborder"><h2 class="subject yellowbg heading">Employment</h2></div>
		<xsl:apply-templates select="job[1]"/>
	</div>
	<xsl:apply-templates select="job[position() > 1]"/>

	<!-- =================== EDUCATION =================== -->
	<div class="page-break-inhibitor">
		<div class="subjectborder"><h2 class="subject bluebg heading">Education</h2></div>
		<xsl:apply-templates select="edu[1]"/>
	</div>
	<xsl:apply-templates select="edu[position() > 1]"/>
	

	<!-- ============== LANGUAGES AND SKILLS ============= -->
	<div class="page-break-inhibitor">
		<div class="subjectborder"><h3 class="subject greenbg heading">Languages and Skills</h3></div>
		<xsl:apply-templates select="skillgroup[1]"/>
	</div>
	<xsl:apply-templates select="skillgroup[position() > 1]"/>


	<!-- =================== INTERESTS =================== -->
	<div class="page-break-inhibitor">
		<div class="subjectborder"><h2 class="subject greenbg heading">Interests</h2></div>
		<xsl:apply-templates select="interest[1]"/>
	</div>
	<xsl:apply-templates select="interest[position() > 1]"/>
	

	<!-- =================== PUBLICATIONS =================== -->
	<div class="page-break-inhibitor">
		<div class="subjectborder"><h2 class="subject redbg heading">Publications</h2></div>
		<xsl:apply-templates select="pub[1]"/>
	</div>
	<xsl:apply-templates select="pub[position() > 1]"/>


	<!-- =================== REFERENCES ================= -->
	<xsl:if test="reference">
		<div class="page-break-inhibitor">
			<div class="subjectborder"><h2 class="subject yellowbg heading">References</h2></div>
			<xsl:apply-templates select="reference"/>	<!-- there'll be so few refs, may as well enclose them all -->

			<!-- also final trailing thing -->
			<div class="endblock content">
				If you're reading this as a PDF or HTML document, you may be curious how it's constructed. The XML and XSLT sources are available at <a href="{repository}"> <xsl:value-of select="repository"/> </a> . <br/>
				I also have a LinkedIn account: <a href="{linkedin}"> <xsl:value-of select="linkedin"/> </a> . <br/>
			</div>
			
		</div>
	</xsl:if>

</body>
</html>

</xsl:template>



<!-- ======================================================================= -->
<xsl:template name="summary">
<!-- ======================================================================= -->
	<div class="summary content">
		<!-- We can do a more wordy write-up of employment in the source xml, since there's a bit of redundancy if we just do a for-each over job elements -->
		<xsl:for-each select="summary">
		<h3 class="summary heading"> <xsl:value-of select="@name"/> </h3>
		<p> <xsl:value-of select="."/> </p>
		</xsl:for-each>

		<h3 class="summary heading"> Education </h3>
		<ul class="horizontal summary">
			<xsl:for-each select="edu[1]">
			<li class="horizontal summary"> <xsl:value-of select="title"/> </li>
			</xsl:for-each>
		</ul>

		<!-- For skills, it's easier to select from our pre-existing list -->
		<h3 class="summary heading"> Skills </h3>
		<ul class="horizontal summary">
			<xsl:for-each select="skillgroup[@id='Loaded' or @id='Learning' or @id='Experienced']">
			<xsl:apply-templates select="skill"/>
			</xsl:for-each>
		</ul>

	</div>

</xsl:template>



<!-- ======================================================================= -->
<xsl:template match="job">
<!-- ======================================================================= -->
	<div class="job content with-divider">
		<h3 class="job heading"> <xsl:value-of select="title"/> </h3>
		<h3 class="job subheading"> <xsl:value-of select="employer"/>, <xsl:value-of select="start"/> - <xsl:value-of select="end"/> </h3>

		<ul class="item">
			<xsl:for-each select="item">
			<li class="item"> <p> <xsl:value-of select="."/> </p> </li>
			</xsl:for-each>
		</ul>
	</div>

</xsl:template>



<!-- ======================================================================= -->
<xsl:template match="edu">
<!-- ======================================================================= -->
	<div class="edu content with-divider">
		<h3 class="edu heading"> <xsl:value-of select="title"/> </h3>
		<h3 class="edu subheading"> <xsl:value-of select="institution"/>, <xsl:value-of select="start"/> - <xsl:value-of select="end"/> </h3>
	
		<ul class="item">
			<xsl:for-each select="item">
			<li class="item"> <p> <xsl:value-of select="."/> </p> </li>
			</xsl:for-each>
		</ul>
	</div>

</xsl:template>



<!-- ======================================================================= -->
<xsl:template match="interest">
<!-- ======================================================================= -->
	<div class="interest content with-divider">
		<h3 class="interest heading"> <xsl:value-of select="title"/> </h3>
		<xsl:for-each select="item">
		<p> <xsl:value-of select="."/> </p>
		</xsl:for-each>
	</div>

</xsl:template>



<!-- ======================================================================= -->
<xsl:template match="skillgroup">
<!-- ======================================================================= -->
	<div class="skillgroup content with-divider">
		<h3 class="skillgroup heading"> <xsl:value-of select="title"/> </h3>
		<h3 class="skillgroup subheading"> <xsl:value-of select="elaboration"/> </h3>
	
		<ul class="item">
			<xsl:apply-templates select="skill"/>
		</ul>
	</div>

</xsl:template>


<!-- =================================== -->
<xsl:template match="skill">
<!-- =================================== -->
	<li class="horizontal skill"> <xsl:value-of select="@name"/> </li>

</xsl:template>





<!-- ======================================================================= -->
<xsl:template match="pub">
<!-- ======================================================================= -->
	<div class="pub content">
		<span class="pubauthors"> <xsl:value-of select="authors"/></span>
		<span class="pubyear"> (<xsl:value-of select="year"/>).</span>

		<span class="pubpaper"> <xsl:value-of select="paper"/></span>

		<xsl:choose>
			<xsl:when test="publication_url">
				in <span class="pubpublication"> <a href="{publication_url}"> <xsl:value-of select="publication"/> </a></span>
			</xsl:when>
			<xsl:otherwise>
				in <span class="pubpublication"> <xsl:value-of select="publication"/></span>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="abbreviation"> <span class="pubabbreviation"> (<xsl:value-of select="abbreviation"/>)</span> </xsl:if>

		<xsl:if test="location"> <span class="publocation"> <xsl:value-of select="location"/></span> </xsl:if>
		<xsl:if test="date"> <span class="pubdate"> <xsl:value-of select="date"/></span> </xsl:if>
		<xsl:if test="pages"> <span class="pubpages"> pp.<xsl:value-of select="pages"/></span> </xsl:if>


		<xsl:choose>
			<xsl:when test="press_url">
				<xsl:if test="press"> <span class="pubpress"> <a href="{press_url}"><xsl:value-of select="press"/></a></span> </xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:if test="press"> <span class="pubpress"> <xsl:value-of select="press"/></span> </xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</div>

</xsl:template>



<!-- ======================================================================= -->
<xsl:template match="reference">
<!-- ======================================================================= -->
	<div class="reference content">
		<h3 class="reference heading"> <xsl:value-of select="name"/> </h3>
		<h3 class="reference subheading"> <xsl:value-of select="location"/> </h3>
		<p> <a href="{homepage}"> <xsl:value-of select="homepage"/> </a> </p>
	</div>

</xsl:template>


</xsl:stylesheet>


