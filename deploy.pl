#!/usr/bin/env perl

use strict;
use warnings;
use v5.16;
use File::Basename;
use Cwd;

chdir dirname $0;
say STDERR "Working dir: " . getcwd;
die "No out/ dir, run yarn build!" unless -d "out";
die "No github/cv/cv/ dir, do a checkout of the github pages site!" unless -d "github/cv/cv";
system("yarn build");
system("rm -Rf github/cv/cv/*");
system("cp -vR out/* github/cv/cv/");
system("cp -v james-clark.pdf github/cv/cv/");

chdir "github/cv";
say STDERR '';
say STDERR "Working dir: " . getcwd;
system("git add .");
system("git commit -m 'latest build'");
system("git push");

say "deploy.pl: Done.";

